# udemy-recipe-app-api-nginx-proxy

Nginx proxy create to efficiently server static files in Django application provided by Udemy DevOps course. 

## Usage

## Environment Variables
  * `LISTEN_PORT` - Port to listen on (default: `8000`)
  * `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
  * `APP_PORT` - Port of the app to forward requests to (default: `9000`)


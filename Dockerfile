FROM nginxinc/nginx-unprivileged:1-alpine
LABEL maintainer="wwq220@gmail.com"

# Copy config files when building the image. By default, Nginx expect all the config files stored in `/etc/nginx`.
COPY ./default.conf.tpl /etc/nginx/default.conf.tpl
COPY ./uwsgi_params /etc/nginx/uwsgi_params

# Define default env variables
ENV LISTEN_PORT=8000
ENV APP_HOST=app
ENV APP_PORT=9000

# Switch to root user
USER root

# Change File Permissions as root user
RUN mkdir -p /vol/static
RUN chmod 755 /vol/static
RUN touch /etc/nginx/conf.d/default.conf
RUN chown nginx:nginx /etc/nginx/conf.d/default.conf

COPY ./entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

# Switch back to ngnix user in order to use ngnix user to run when docker container starts.
USER nginx

# Default command to run when docker is started.
CMD ["/entrypoint.sh"]



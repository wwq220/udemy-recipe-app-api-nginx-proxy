#!/bin/sh

# Throw errors to the screen if any script execution failed for debugging purpose.
set -e

# Environment Substitude: Inject env var variable into default.conf.tpl file and generate default.conf file with the env var values.
envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf

# Start the Nginx in foreground with daemon off.
# By default, Ngnix runs in background with daemon. 
# We want Ngnix server to run in the docker foreground to reveal console logs.
nginx -g 'daemon off;'